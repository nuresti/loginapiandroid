package com.whaapps.loginapi.network;

import com.whaapps.loginapi.model.Users;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by whaapps on 07/05/18.
 */

public interface MyService {
    @FormUrlEncoded
    @POST("loginAPI.php")
    Call<Users> login(@Field("email")String email,
                        @Field("password")String password);

}
