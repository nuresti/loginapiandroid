package com.whaapps.loginapi.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.whaapps.loginapi.R;
import com.whaapps.loginapi.model.Users;
import com.whaapps.loginapi.network.MyService;
import com.whaapps.loginapi.network.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    //TODO membuat field atau global variable
    EditText username, password;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //TODO Integrasi Field dengan id di XML
        username = findViewById(R.id.etUsername);
        password = findViewById(R.id.etPassword);
        login = findViewById(R.id.BtLogin);
        // TODO interface method pake new spasi control space
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<Users> call  = ServiceGenerator.createService(MyService.class).login(username.getText().toString(),
                        password.getText().toString());
                call.enqueue(new Callback<Users>() {
                    @Override
                    public void onResponse(Call<Users> call, Response<Users> response) {
                        int value = response.body().value;
                        String level = response.body().level;
                        if(value == 1){
                            if(level.equals("1")){
                                startActivity(new Intent(Login.this, AdminActivity.class));
                            }else if (level.equals("2")){
                                startActivity(new Intent(Login.this, MemberActivity.class));
                            }
                        }else
                            Toast.makeText(Login.this, "Username dan Password Salah", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Users> call, Throwable t) {
                        Toast.makeText(Login.this, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
