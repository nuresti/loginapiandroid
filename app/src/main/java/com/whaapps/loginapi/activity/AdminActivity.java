package com.whaapps.loginapi.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.whaapps.loginapi.R;

public class AdminActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
    }
}
